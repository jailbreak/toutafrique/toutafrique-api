# ToutAfrique API

Serve indicators and topologies.

## Install

Use a virtualenv:

```bash
virtualenv venv
source venv/bin/activate
```

Then install dependencies and package:

```bash
pip install -r requirements.txt
pip install -e .
```

## Configure

```bash
cp .env.example .env
```

Then adjust the settings.

### `DATA_FS_URL`

The API needs to access data to serve its routes.

Thanks to [pyfilesystem](https://docs.pyfilesystem.org/), the API can read its data from any supported filesystem. For example, a local directory or a remote S3 bucket (with the [S3FS plugin](https://fs-s3fs.readthedocs.io/)).

In production, data is stored in a public S3 bucket. The `DATA_FS_URL` variable in `.env.example` is already set to use it, but you still have to update `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`.

This allows to work on the source code of the API.

#### Sync data locally

This is optional: do this only if you'd like to download data locally, for example to work offline or to work on [toutafrique-data-processing](https://framagit.org/jailbreak/toutafrique/toutafrique-data-processing/).

You can use a tool like `awscli` or `s3cmd` to sync the S3 bucket on your machine. Example with awscli:

```ini
; This is the content of awscli.ini

; From https://www.scaleway.com/en/docs/object-storage-with-aws-cli/

[plugins]
endpoint = awscli_plugin_endpoint

[default]
region = fr-par
s3 =
  endpoint_url = https://s3.fr-par.scw.cloud
  signature_version = s3v4
  max_concurrent_requests = 100
  max_queue_size = 1000
  multipart_threshold = 50MB
  # Edit the multipart_chunksize value according to the file sizes that you want to upload. The present configuration allows to upload files up to 10 GB (100 requests * 10MB). For example setting it to 5GB allows you to upload files up to 5TB.
  multipart_chunksize = 10MB
s3api =
  endpoint_url = https://s3.fr-par.scw.cloud
```

```bash
pip install awscli awscli_plugin_endpoint
AWS_CONFIG_FILE=awscli.ini aws s3 sync --no-sign-request s3://afd-toutafrique-data data
```

The `data` directory now contains the latest production data. You can launch this command again to refresh.

Update `DATA_FS_URL` to `./data` to let the application know about the data new location.

## Run

```bash
./serve.sh
```

Visit http://localhost:8000 in your web browser

Or check the <a href="http://127.0.0.1:8000/docs">documentation</a>

## Production

### Build Docker image

For now Docker image building is manual:

```bash
docker build -t jailbreakparis/toutafrique-api .
```

Test with:

```bash
docker run --name toutafrique-api --rm -p 8000:80 -v $PWD/data:/app/toutafrique-data jailbreakparis/toutafrique-api
```

Then, if it's OK:

```bash
docker push jailbreakparis/toutafrique-api
```

### Deployment

Cf https://framagit.org/jailbreak/toutafrique/toutafrique-docker

### Environment variables

Environment variables can be defined via docker-compose (cf link above) via a `.env` file.

- to enable Sentry, set `SENTRY_DSN` to an URL like `https://xxx@yyy.ingest.sentry.io/zzz` that you can get from Client Keys settings page (`https://sentry.io/settings/<user_id>/projects/<project_id>/keys/`)
