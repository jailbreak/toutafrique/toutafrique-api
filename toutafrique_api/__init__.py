# toutafrique-api: Tout Afrique Web API
# By: Jailbreak Team <team@jailbreak.paris>
#
# Copyright (C) 2020 Jailbreak
# https://framagit.org/jailbreak/toutafrique/toutafrique-api
#
# toutafrique-api is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# toutafrique-api is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


"""Init API server."""


from enum import Enum
from urllib.parse import urljoin
from typing import List

import fs
import pandas as pd
from fastapi import FastAPI, Query, Request
from fastapi.middleware.cors import CORSMiddleware
from fastapi.middleware.gzip import GZipMiddleware

from . import config, model
from .util import (
    IndicatorsInfo,
    build_indicators_for_area,
    build_population_data,
    compose_response_function,
    load_json_file,
)

app = FastAPI(
    title="toutafrique-api",
    description="Serve data for ToutAfrique dashboard",
    version=config.API_VERSION,
    root_path=config.API_BASE_PATH,
)

# CORS
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# GZip
app.add_middleware(GZipMiddleware)

if config.SENTRY_DSN:
    import sentry_sdk
    from sentry_sdk.integrations.asgi import SentryAsgiMiddleware

    sentry_sdk.init(dsn=config.SENTRY_DSN)
    app.add_middleware(SentryAsgiMiddleware)


@app.on_event("shutdown")
def shutdown_event():
    """Close FS on shutdown."""
    config.DATA_FS.close()


ok_response, err_response = compose_response_function(config.API_VERSION)


@app.get("/", include_in_schema=False)
def index(request: Request):
    """Home page."""
    docs_url = urljoin(str(request.url), app.docs_url)
    return ok_response(
        {
            "message": (
                "This is the home page of toutafrique API."
                f" Its documentation is there: {docs_url}"
            )
        },
    )


@app.get("/indicators")
def indicators():
    """Return indicator list."""
    indicators_info = IndicatorsInfo(config.DATA_FS, "indicators_info.json")
    indicators_data = indicators_info.get_json_content()
    return ok_response(indicators_data)


class AreaSlug(str, Enum):
    """Slug values for geographical areas."""

    world = "world"
    africa = "africa"
    africa_dr = "africa_dr"


@app.get("/indicators/area/{area_slug}/{indicator_slug}")
def indicators_for_area(
    area_slug: AreaSlug, indicator_slug: str, remove_na: bool = True,
):
    """Return an indicator for each territory of an area."""
    query_args = {
        "indicator_slug": indicator_slug,
        "area": area_slug,
    }

    csv_file = fs.path.combine("csv_data", f"{indicator_slug}_{area_slug}.csv")
    if not config.DATA_FS.exists(csv_file):
        return err_response(
            "Indicator data not found", args=query_args, status_code=404,
        )

    indicators_info = IndicatorsInfo(config.DATA_FS, "indicators_info.json")

    with config.DATA_FS.open(csv_file) as f:
        result = build_indicators_for_area(
            area_slug, indicators_info, indicator_slug, f, remove_na
        )

    return ok_response(result, args=query_args)


@app.get("/indicators/territories")
def indicators_by_territories(territory_codes: List[str] = Query(..., alias="code")):
    """Get all indicators by territory, accepting many territories.

    Each territory code can be either:
    - a continent code: only AF is supported
    - a country ISO alpha-3 code
    - a DR (direction régionale) code

    Note: continent codes come from
    https://datahub.io/core/continent-codes#resource-continent-codes
    """

    query_args = {"territory_codes": territory_codes}

    territory_info_json = load_json_file(config.DATA_FS, "territory_info.json")
    territories_model = model.Territories.parse_obj(territory_info_json)

    indicators_df = pd.DataFrame()

    for territory_code in territory_codes:
        try:
            territory_type = territories_model.get_territory_type(territory_code)
        except ValueError as exc:
            return err_response(exc, args=query_args, status_code=404)

        json_file = fs.path.combine(
            f"{territory_type.lower()}_data", f"{territory_code}_indicators.json"
        )
        if not config.DATA_FS.exists(json_file):
            return err_response(
                f"Data not found for territory code {territory_code!r}",
                args=query_args,
                status_code=404,
            )

        territory_indicators = load_json_file(config.DATA_FS, json_file)
        for indicator_slug, observations in territory_indicators["indicators"].items():
            indicator_territory_df = pd.DataFrame(
                observations, columns=["period", "value"]
            )
            indicator_territory_df["indicator_slug"] = indicator_slug
            indicator_territory_df["territory_code"] = territory_code
            indicators_df = pd.concat([indicators_df, indicator_territory_df])

    indicators_by_slug = {
        indicator_slug: {
            territory_code: sub_df2.drop("territory_code", axis=1).to_dict(
                orient="list"
            )
            for territory_code, sub_df2 in sub_df.drop(
                "indicator_slug", axis=1
            ).groupby("territory_code")
        }
        for indicator_slug, sub_df in indicators_df.groupby("indicator_slug")
    }

    return ok_response(indicators_by_slug, args=query_args)


@app.get("/population_by_country/{country_code}")
def population_by_country(
    country_code: str = Query(..., min_length=3, max_length=3, regex=r"^[A-Z]{3}$"),
):
    """Get population info by country iso alpha-3 code (for african countries only)."""
    query_args = {
        "country_code": country_code,
    }

    csv_file = fs.path.combine("pop_data", f"pop_{country_code}.csv")
    if not config.DATA_FS.exists(csv_file):
        return err_response(
            f"Unknown country code [{country_code}]", args=query_args, status_code=404,
        )
    with config.DATA_FS.open(csv_file) as f:
        result = build_population_data(country_code, f)
        return ok_response(result, args=query_args)


@app.get("/territories")
def territories():
    """Load metadata about territories (countries, DR)."""
    territory_info_json = load_json_file(config.DATA_FS, "territory_info.json")
    return ok_response(territory_info_json)


@app.get("/topologies")
def topologies():
    """Return a list of available GeoJSON topologies."""
    with config.DATA_FS.opendir("topologies") as topologies_dir:
        topologies = [
            {"slug": match.info.stem} for match in topologies_dir.glob("*.geojson")
        ]
    return ok_response(topologies)


@app.get("/topologies/{area_slug}")
def topology(area_slug: AreaSlug):
    """Return a GeoJSON topology."""
    query_args = {"area_slug": area_slug}
    topology_file = fs.path.combine("topologies", f"{area_slug}.geojson")
    topology = load_json_file(config.DATA_FS, topology_file)
    return ok_response(topology, args=query_args)
