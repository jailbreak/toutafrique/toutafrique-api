# toutafrique-api: Tout Afrique Web API
# By: Jailbreak Team <team@jailbreak.paris>
#
# Copyright (C) 2020 Jailbreak
# https://framagit.org/jailbreak/toutafrique/toutafrique-api
#
# toutafrique-api is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# toutafrique-api is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


"""Config informations."""


import logging
import os

import fs
import fs.wrap
import pkg_resources
from dotenv import load_dotenv
from fs.base import FS

# Logger
log = logging.getLogger(__name__)

# Load settings from .env file if exists
load_dotenv()

# Logger config
LOG_LEVEL = os.environ.get("LOG_LEVEL", "WARNING").upper()
logging.basicConfig(level=LOG_LEVEL)

# API config
API_BASE_PATH = os.environ.get("API_BASE_PATH")

# API version
try:
    API_VERSION = pkg_resources.get_distribution("toutafrique-api").version
except pkg_resources.DistributionNotFound:
    API_VERSION = "development"


def load_data_fs() -> FS:
    """Load data from agnostic filesystem."""
    data_fs_url = os.environ.get("DATA_FS_URL")
    if data_fs_url is None:
        raise ValueError("DATA_FS_URL environment variable is not set")
    data_fs = fs.open_fs(data_fs_url)
    return fs.wrap.read_only(data_fs)


# Where to find ToutAfrique data
DATA_FS = load_data_fs()

SENTRY_DSN = os.environ.get("SENTRY_DSN")
