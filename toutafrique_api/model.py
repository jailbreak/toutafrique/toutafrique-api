# toutafrique-api: Tout Afrique Web API
# By: Jailbreak Team <team@jailbreak.paris>
#
# Copyright (C) 2020 Jailbreak
# https://framagit.org/jailbreak/toutafrique/toutafrique-api
#
# toutafrique-api is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# toutafrique-api is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


"""Model definition."""

from typing import List, Literal

from pydantic import BaseModel

from .util import find


TerritoryCode = str
TerritoryType = Literal["continent", "country", "DR"]


class Areas(BaseModel):
    africa: List[TerritoryCode]
    africa_dr: List[TerritoryCode]
    world: List[TerritoryCode]


class Territory(BaseModel):
    code: TerritoryCode
    name: str
    type: TerritoryType


class Territories(BaseModel):
    areas: Areas
    territories: List[Territory]

    def get_territory_type(self, territory_code: TerritoryCode) -> TerritoryType:
        try:
            territory = find(
                lambda territory: territory.code == territory_code, self.territories
            )
        except ValueError as exc:
            raise ValueError(f"Territory code {territory_code!r} not found") from exc
        return territory.type
